"-----------------
" other keybind
"-----------------
nnoremap <silent><ESC><ESC> :<C-u>noh<CR>
inoremap <silent> jj <ESC>

nnoremap <silent><C-n> :<C-u>cn<CR>
nnoremap <silent><C-p> :<C-u>cp<CR>

nnoremap s <Nop>
nnoremap sj <C-w>j
nnoremap sk <C-w>k
nnoremap sl <C-w>l
nnoremap sh <C-w>h
nnoremap sJ <C-w>J
nnoremap sK <C-w>K
nnoremap sL <C-w>L
nnoremap sH <C-w>H
nnoremap sr <C-w>r
nnoremap s= <C-w>=
nnoremap sw <C-w>w
nnoremap so :<C-u>only<CR>
nnoremap sO <C-w>=
nnoremap ss :<C-u>sp<CR>
nnoremap sv :<C-u>vs<CR>
nnoremap sq :<C-u>q<CR>

"-----------------
" mizawa's keybind
"-----------------
nnoremap k gk
nnoremap j gj
nnoremap <Down> gj
nnoremap <Up> gk
nnoremap <silent> <space>o :<C-u>only<CR>
nnoremap <silent><buffer>q :quit<CR>

